+++
title = "KiCad Donate FAQ"
summary = "Frequently Asked Questions about Donating to KiCad"
weight = 2
aliases = [ "/donate/faq/" ]
+++

[.initial-text]
== Frequently Asked Questions

++++
<div class="accordion accordion-flush" id="accordion" role="tablist" aria-multiselectable="false">
{{< faq "One" "How are the donations used?" >}} All donations are used to develop the KiCad EDA.  We hire programmers, technical writers and UX engineers to make the next version of KiCad better than ever. {{< /faq >}}

{{< faq "Two" "Can I receive a charitable receipt for my donation?" >}} No.  KiCad works in partnership with KiCad Services Corporation and the Linux Foundation.  Neither is a charitable organization under US tax law. {{< /faq >}}

{{< faq "Three" "What currencies can I donate in?" >}} KiCad accepts donations in US dollars, Euros, Pounds, Rupees, Yen and Yuan.  If you would like KiCad to accept your local currency, <a href='mailto:donations@kicad.org?subject=New Currency'>please reach out to us</a>. {{< /faq >}}

{{< faq "Four" "What methods of payment are supported?" >}} Donations are accepted by credit card, Apple Pay and Google Pay in all currencies.  If donating in Yuan, you may also select AliPay.  If donating in Euro, you may also select SOFORT, iDEAL, giropay or SEPA debit {{< /faq >}}

{{< faq `Five` `What should I do if I made a mistake when submitting my donation?` >}} Please <a href='mailto:donations@kicad.org?subject=Donation Error'>e-mail us</a> with all of your contact information and and explaination of what you need corrected. {{< /faq >}}

{{< faq "Six" "What amounts may I contribute?" >}} We are grateful for any amount you contribute.  You can type in a custom amount or use one of the preset suggestions. {{< /faq >}}

{{% faq "Seven" "How much of my contribution goes to KiCad?" %}} 100% of the contribution received goes to developing KiCad and the KiCad community.  But we do have some overhead in payment processing.  <ul><li>Our payment processor charges 2.9% + 30¢ for US$ transactions.  <li>Credit card transactions and AliPay in non-US currency cost 4.9% + 30¢.  <li>Payments via iDEAL, SOFORT, giropay and SEPA are more efficient and charge between 80¢ and 2.4% + 30¢.</ul>` {{% /faq %}}

{{< faq "Eight" "Can I donate anonymously?" >}} Yes.  You can choose to donate anonymously.  We will not publish your name or contact information, however we do maintain the minimum amount of information needed to verify the transaction.  This information is kept confidential. {{< /faq >}}

{{< faq "Nine" "Can I donate cryptocurrency?" >}} No.  We do not accept donations in the form of crypto currency and do not anticipate doing so at any point in the future. {{< /faq >}}

{{< faq "Ten" "Can I donate via IBAN?" >}} Yes.  We greatly appreciate IBAN donations as your full donation amount is received.  You can send donations to <code>BE81 9672 4527 0324</code>.  The account holder is <code>KiCad Services Corporation</code>.  Please include your e-mail address in the memo field if you would like a receipt. {{< /faq >}}

</div>
++++
