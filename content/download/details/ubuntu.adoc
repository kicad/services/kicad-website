+++
title = "Install on Ubuntu"
summary = "Install instructions for KiCad on Ubuntu"
+++

== {{< param "release" >}} Stable Release

=== Installation
KiCad {{< param "release" >}} is available in https://launchpad.net/~kicad/+archive/ubuntu/kicad-8.0-releases[PPA for KiCad: 8.0 releases ].
include::./content/download/_kicad_ppa.adoc[]


== 7.0 Old Stable Release

KiCad 7.0 is available in https://launchpad.net/~kicad/+archive/ubuntu/kicad-7.0-releases[PPA for KiCad: 7.0 releases].
