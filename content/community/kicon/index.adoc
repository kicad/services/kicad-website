+++
title = "KiCad Conference Series"
aliases = [ "/community/kicon/" ]
[menu.main]
    parent = "Community"
    name   = "KiCon"
    weight = 15
+++
:linkattrs:

The KiCad Conference Series is a yearly conference that takes place in the fall.
The series brings together people who use and love KiCad to share ideas, triumphs,
learnings and more.

image::badges.png[width=400]
---
== 2020-2022 Retrospective

During the coronavirus pandemic, the KiCad conference series was held 
exclusively online

image::2020.jpg[width=400]

image::2022.jpg[width=400]

---
== 2019 Retrospective

The 2019 KiCon was held on April 26th and 27th in Chicago, IL, USA.

image::crowd1.png[width=600]

More than 250 attendees crowded into a sold-out hall at M-Hub in Chicago to 
learn and teach KiCad.

image::speaker1.jpg[width=250]

Speakers convered the basics of KiCad collaboration, design, layout and working with
your fabricators.

image::people1.jpg[width=450]

Many PCB fabrication houses sponsored the event and sent representatives to meet their
customers and share experiences in the circuit design industry.

image::speaker2.jpg[width=350]

The talks even delved deep into coding circuit design for reuse and topics
that you could not hear anywhere else.

image::people2.jpg[width=400]

Between sessions, attendees browsed the display hall where vendors set up tables to 
show their tools and resources to help circuit designers with their jobs

image::speaker3.jpg[width=450]

The whole two days were packed with more information than you could consume in a weekend.  
But even the talks that you missed are available for later consumption on YouTube through
our dedicated list

video::nL0yTvJKA5c/PLy2022BX6EsqLkQy1EmXjVnauOH3FSHTV[youtube]