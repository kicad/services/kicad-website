const fundBanner = document.querySelector('.fund-banner');
const closeBannerBtn = document.querySelector('.fund-banner__close-btn');
const header = document.querySelector('.navbar');
const closeCookieName = 'fund-drive-2025-closed';

closeBannerBtn.addEventListener('click', function () {
  fundBanner.style.display = 'none';
  header.style.top = '0';
  document.body.style.paddingTop = '0';

  setCookie(closeCookieName, '1', 30);
});

window.addEventListener('scroll', function () {
  if (window.scrollY == 0) {
    fundBanner.style.position = '';
  } else {
    fundBanner.style.position = 'fixed';
  }
});

function calculateAmount() {
  const totalValue = $('.fund-banner__bar-total').text().trim();
  const totalValueResult = Number(totalValue.replace(/[^0-9.-]+/g,""));

  const currentValue = $('.fund-banner__bar-current-sum').text().trim();
  const currentValueResult = Number(currentValue.replace(/[^0-9.-]+/g,""));

  document.documentElement.style.setProperty(
    '--total-amount',
    totalValueResult
  );
  document.documentElement.style.setProperty(
    '--current-amount',
    currentValueResult
  );
}

document.addEventListener('DOMContentLoaded', (event) => {
  if ($('.fund-banner').hasClass('transparent')) {
    $(window).scroll(function () {
      if ($(this).scrollTop() < 430) {
        $('.fund-banner').addClass('transparent');
      } else {
        $('.fund-banner').removeClass('transparent');
      }
    });
  }

  calculateAmount();
  var closeState = getCookie(closeCookieName);
  if(closeState != null && closeState == '1')
  {
    closeBannerBtn.click();
  }
})
